<section class="content-header">
  <h1>
    Agendar SMS
   	<small>Agendar SMS</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	<li><a href="#"> Agendar SMS</a></li>
    <li class="active">Agendar SMS</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
        		<?php echo $this->Form->create('ScheduledMessage'); ?>
   					
   					<?php echo $this->Form->hidden( 'client_id');?>
   					<?php echo $this->Form->hidden( 'message_id');?>
   					<?php echo $this->Form->hidden( 'schedule');?>
   					<?php echo $this->Form->hidden( 'status');?>
   					<?php echo $this->Form->hidden( 'created');?>
   					<?php echo $this->Form->hidden( 'modified');?>
   					<div class="col-md-12">
						<div class="box box-default">
							<div class="box-header with-border">
								<h3 class="box-title">Adicionar Mensagem</h3>
							</div><!-- /.box-header -->
							<div class="box-body">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											 <?php echo $this->Form->input( 'id', ['label' => 'ID da Mensagem', 'class' => 'form-control']);?>
										</div><!-- /.form-group -->
									</div><!-- /.col -->
								</div><!-- /.row -->
							</div><!-- /.box-body -->
					        <div class="box-footer">
					            <?php
					              echo '&nbsp;';
					              echo '&nbsp;';
					              echo '&nbsp;';
					              echo $this->Form->button(
					                  '<i class="fa fa-check"></i> Avançar',
					                  array('class' => 'btn btn-success')
					              );
					            ?>
					        </div>						
    					</div><!-- /.box -->
					</div>
				</div>
        	<?php echo $this->Form->end(); ?>
		</div>
	</div>
</section>

<!-- Bootstrap Color Picker -->
<?php echo $this->Html->css(array('plugins/colorpicker/bootstrap-colorpicker.min'));?>

<!-- jQuery 2.1.4 -->
<?php echo $this->Html->script('plugins/jQuery/jQuery-2.1.4.min.js');?>
<!-- Bootstrap 3.3.5 -->
<?php echo $this->Html->script('bootstrap/js/bootstrap.min'); ?>
<!-- FastClick -->

<!-- bootstrap color picker -->
<?php echo $this->Html->script('plugins/colorpicker/bootstrap-colorpicker.min'); ?>

<!-- AdminLTE App -->
<?php echo $this->Html->script('dist/js/app.min'); ?>
