<?php
  echo $this->element('navegacao');
?>

<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-md-3">
			<!-- Profile Image -->
			<div class="box box-primary">
				<div class="box-body box-profile">
					<?php echo $this->Html->image('/files/' . $this->data['User']['photo'], array('alt' => 'User Image', 'class' => 'profile-user-img img-responsive img-circle'));?>
					<h3 class="profile-username text-center"><?php echo $this->data['User']['name']; ?></h3>
					<!-- <p class="text-muted text-center">Software Engineer</p> -->
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div><!-- /.col -->
		<div class="col-md-9">
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<!-- <li class="active"><a href="#activity" data-toggle="tab">Activity</a></li> -->
					<!-- <li><a href="#timeline" data-toggle="tab">Timeline</a></li> -->
					<li class="active"><a href="#settings" data-toggle="tab">Configurações</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="settings">
						<?php 
							echo $this->Form->create('User', array('class' => 'form-horizontal', 'type' => 'file', 'url' => array('action' => 'profile'))); 
								echo $this->Form->input('id');
								echo $this->Form->hidden('photo_h', array('value' => $this->data['User']['photo']));
						?>
							<div class="form-group">
								<label for="inputName" class="col-sm-2 control-label">Nome</label>
								<div class="col-sm-10">
									<?php 
										echo $this->Form->input('name', array('class' => 'form-control', 'label' => false, 'div' => false, 'placeholder' => 'Nome'));
									?>									
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail" class="col-sm-2 control-label">E-mail</label>
								<div class="col-sm-10">
									<?php 
										echo $this->Form->input('username', array('class' => 'form-control', 'label' => false, 'div' => false, 'placeholder' => 'Nome'));
									?>									
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail" class="col-sm-2 control-label">Senha</label>
								<div class="col-sm-10">
									<?php 
										echo $this->Form->input('password', array('class' => 'form-control', 'label' => false, 'div' => false, 'placeholder' => 'Senha', 'value' => ''));
									?>									
								</div>
							</div>
							<div class="form-group">
								<label for="inputName" class="col-sm-2 control-label">Foto</label>
								<div class="col-sm-10">
									<?php 
										echo $this->Form->file('photo', array('class' => 'form-control', 'label' => false, 'div' => false));
									?>									
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Salvar</button>
								</div>
							</div>
						<?php echo $this->Form->end(); ?>
					</div><!-- /.tab-pane -->
				</div><!-- /.tab-content -->
			</div><!-- /.nav-tabs-custom -->
		</div><!-- /.col -->
	</div><!-- /.row -->
</section><!-- /.content -->
<!-- jQuery 2.1.4 -->
<?php echo $this->Html->script('plugins/jQuery/jQuery-2.1.4.min.js');?>
<!-- Bootstrap 3.3.5 -->
<?php echo $this->Html->script('bootstrap/js/bootstrap.min'); ?>
<!-- FastClick -->
<!-- AdminLTE App -->
<?php echo $this->Html->script('dist/js/app.min'); ?>

<!-- Sparkline -->
<?php echo $this->Html->script('plugins/sparkline/jquery.sparkline.min.js');?>
<!-- jvectormap -->
<?php echo $this->Html->script('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js');?>
<?php echo $this->Html->script('plugins/jvectormap/jquery-jvectormap-world-mill-en.js');?>
<!-- SlimScroll 1.3.0 -->
<?php echo $this->Html->script('plugins/slimScroll/jquery.slimscroll.min.js');?>
<!-- ChartJS 1.0.1 -->
<?php echo $this->Html->script('plugins/chartjs/Chart.min.js');?>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<?php echo $this->Html->script('dist/js/pages/dashboard2'); ?>

<!-- AdminLTE for demo purposes -->
<?php echo $this->Html->script('dist/js/demo'); ?>