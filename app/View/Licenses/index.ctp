
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Configurações dos Planos</h3>
					<div class="box-tools" style="display:none;">
						<div class="input-group" style="width: 150px;">
							<input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
							<div class="input-group-btn">
								<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
							</div>
						</div>
					</div>
				</div><!-- /.box-header -->
				<div class="box-body">

					<?php foreach($licenses as $license) : ?>
						<a href="<?php echo $this->Html->url(['action' => 'edit', $license['License']['id'] ]); ?>" class="col-xs-5 col-xs-offset-1 text-center btn btn-default" style="height: 250px; line-height: 45px; font-size: 24px;">
							<strong><?php echo $license['License']['title']; ?></strong>
							<div class="clearfix"></div>
							<small>Limite de Produtos <strong><?php echo $license['License']['product_limit']; ?></strong></small>
							<div class="clearfix"></div>
							<small>Limite de Mensagens <strong><?php echo $license['License']['message_limit']; ?></strong></small>
							<div class="clearfix"></div>
							<small>Total de Usuários Usando <strong><?php echo count($license['User']); ?></strong></small>
							<div class="clearfix"></div>
							<span class="label label-success">Clique para editar</span>
						</a>
					<?php endforeach; ?>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>
</section>
