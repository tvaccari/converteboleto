
<!-- Main content -->
<section class="content">
	<div class="row">


		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Edição de Plano</h3>
					<a href="<?php echo $this->Html->url(['action' => 'add',  ]); ?>" class="btn btn-lg btn-success pull-right"><i class="fa fa-arrow-left"></i> Voltar</a>
				</div> <!-- .box-header -->

				<div class="box-body">

					<?php echo $this->Form->create("License"); ?>

					<?php echo $this->Form->input("id"); ?>

					<div class="form-group col-xs-6">
						<?php echo $this->Form->input("title", ['label' => 'Nome', 'class' => 'form-control']); ?>
					</div>

					<div class="form-group col-xs-6">
						<?php echo $this->Form->input("product_limit", ['label' => 'Limite de Produtos', 'class' => 'form-control']); ?>
					</div>

					<div class="form-group col-xs-6">
						<?php echo $this->Form->input("message_limit", ['label' => 'Limite de Mensagens', 'class' => 'form-control']); ?>
					</div>

					<div class="form-group col-xs-6">
						<?php echo $this->Form->button("Salvar", ['class' => 'btn btn-success btn-block']); ?>
					</div>

					<?php echo $this->Form->end(); ?>

				</div> <!-- .box-body -->
			</div> <!-- .box -->
		</div> <!-- .col -->

	</div> <!-- .row -->
</section> <!-- .content -->