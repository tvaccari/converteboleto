<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <div class="user-panel" style="height: 60px;">
      <div class="pull-left info">
        <p><?php echo AuthComponent::user('name');?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
        <a href="<?php echo $this->Html->url(['action' => 'logout', 'controller' => 'login']); ?>" class="label label-danger pull-right"><i class="fa fa-sign-out"></i> Sair</a>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">NAVEGAÇÃO</li>

      <li class="<?php if(@$menu == 'products') echo 'active'; ?>">
        <?php
          echo $this->Html->link(
              '<i class="fa fa-inbox"></i> <span>Produtos</span> ',
              '/products/index',
              array('escape' => false)
          );
        ?>
      </li>

      <li class="<?php if(@$menu == 'users') echo 'active'; ?>">
        <?php
          echo $this->Html->link(
              '<i class="fa fa-inbox"></i> <span>Usuários do Sistema</span> ',
              '/users/index',
              array('escape' => false)
          );
        ?>
      </li>

      <li class="<?php if(@$menu == 'licenses') echo 'active'; ?>">
        <?php
          echo $this->Html->link(
              '<i class="fa fa-inbox"></i> <span>Configurações dos Planos</span> ',
              '/licenses/index',
              array('escape' => false)
          );
        ?>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
