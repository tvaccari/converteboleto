<?php

App::uses('AppController', 'Controller');

class InfobipController extends AppController {

	private $curl, $base;

/**
 * beforeFilter method
 *
 * @return void
 */
	public function beforeFilter() {
		parent::beforeFilter();
		
		$this->autoRender = false;
		$this->Auth->allow();

		// Infobid
		$this->base = 'https://api.infobip.com/';
		$credential = "bendit" . ":" . "2F4bd72J";
        $this->curl = curl_init();
        $header[] = "Authorization: Basic " . base64_encode($credential);
        $header[] = "Content-Type:  application/json";
        $header[] = "Accept:        application/json";
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($this->curl, CURLINFO_HEADER_OUT, true);
	}

/**
 * index method
 *
 * @return void
 */
	public function index(){}

/**
 * send method
 *
 * @return void
 */
	public function send( $data = array() ) {
		//$url = "sms/1/text/single";
		$url = "api/v3/sendsms/json";

		$data = array( 'from' => 'CB', 'to' => '5512981250819', 'text' => '1 SMS via Sistema' );

        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, 'POST');           
        curl_setopt($this->curl, CURLOPT_URL, $this->base . $url);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, json_encode($data));
        $response  = curl_exec($this->curl);
        $http_code = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
        pr( $http_code );
		pr( $response );
		exit;
	}

	public function getLog( $id = null ) {
		$url = 'sms/1/logs';

        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, 'GET');           
        curl_setopt($this->curl, CURLOPT_URL, $this->base . $url);
        $response  = curl_exec($this->curl);
        $http_code = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
        pr( $http_code );
		pr( $response );
		exit;
	}
}
?>