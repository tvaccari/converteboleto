<?php
App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');
/**
 * Arquivo Model
 *
 * @property Aula $Aula
 */
class User extends AppModel {

	public $validate = array(
		'name' => array(
			'notEmpty'
		),
		'username' => array(
			'notEmpty',
			'isUnique' => array(
				'rule' => 'isUnique',
				'message' => 'Este usuário já existe. Utilize outro.'
			)
		),
		'password' => array(
			'minLength' => array(
				'rule' => array('minLength', 6),
				'message' => 'Mínimo de 6 caracteres.',
				'on' => 'create'
			)
		)
	);

	public function beforeSave($options = array()) {
		
		if(empty($this->data['User']['password']))
			unset($this->data['User']['password']);
		else
			$this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
	    
	    return true;
	}

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'License' => array(
			'className' => 'License',
			'foreignKey' => 'license_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);
}
