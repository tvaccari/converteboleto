<?php
App::uses('AppModel', 'Model');

class Billet extends AppModel {

	public $recursive = 2;

	public $belongsTo = array(
		'Transaction' => array(
			'className' => 'Transaction',
			'foreignKey' => 'transaction_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
